class Food {
  final String name;
  final double price;
  final List<int> comp;
  final String image;
  final String desc;
  final int time;

  Food(this.name, this.price, this.comp, this.image, this.desc, this.time);
}
