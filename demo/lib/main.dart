import 'package:demo/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
    );

    return MaterialApp(
      theme: ThemeData(
          scaffoldBackgroundColor: const Color(0xf4f5f7ff),
          fontFamily: 'Neogrotesk'),
      debugShowCheckedModeBanner: false,
      title: 'Test Task',
      home: const HomePage(),
    );
  }
}
