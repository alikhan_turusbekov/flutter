import 'package:demo/screens/home/widgets/body.dart';
import 'package:demo/screens/home/widgets/header.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: const [
          Header(),
          Body(),
        ],
      ),
    );
  }
}
