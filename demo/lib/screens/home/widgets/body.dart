import 'package:demo/screens/home/widgets/catalog.dart';
import 'package:demo/screens/home/widgets/filter.dart';
import 'package:demo/screens/footer.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
      height: queryData.size.height * 0.78,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 15.0,
            offset: Offset(
              0.0,
              -3.0,
            ),
          ),
        ],
      ),
      child: Column(
        children: [
          const Filter(),
          Catalog(),
          const Footer(text: '30 min'),
        ],
      ),
    );
  }
}
