import 'package:flutter/material.dart';

class Filter extends StatefulWidget {
  const Filter({Key? key}) : super(key: key);

  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<Filter> {
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    List<bool> isSelected = [true, false];

    return Container(
      width: double.infinity,
      height: queryData.size.height * 0.10,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 15.0,
            offset: Offset(
              0.0,
              -5.0,
            ),
          ),
        ],
      ),
      child: Container(
          padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
          width: 300,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 40,
                child: ToggleButtons(
                  children: const <Widget>[
                    Icon(
                      Icons.check_box_outline_blank_outlined,
                      size: 18,
                    ),
                    Icon(
                      Icons.view_list_outlined,
                      size: 18,
                    ),
                  ],
                  onPressed: (int index) {
                    setState(() {});
                  },
                  isSelected: isSelected,
                  selectedColor: Colors.redAccent,
                  borderRadius: BorderRadius.circular(8.0),
                  fillColor: const Color(0xf4f5f7ff),
                ),
              ),
              ElevatedButton.icon(
                onPressed: () {
                  // Respond to button press
                },
                icon: const Icon(
                  Icons.free_breakfast_outlined,
                  size: 18,
                  color: Colors.redAccent,
                ),
                label: const Text(
                  "Breakfast",
                  style: TextStyle(color: Colors.redAccent),
                ),
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xffffcdd2),
                ),
              ),
              ElevatedButton.icon(
                onPressed: () {
                  // Respond to button press
                },
                icon: const Icon(
                  Icons.ramen_dining_outlined,
                  size: 18,
                  color: Colors.black,
                ),
                label: const Text(
                  "Noodles",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xf4f5f7ff),
                ),
              ),
            ],
          )),
    );
  }
}
