import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return SizedBox(
      width: double.infinity,
      height: queryData.size.height * 0.22,
      child: Center(
        child: Container(
          margin: const EdgeInsets.only(
            top: 20,
          ),
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(top: 15),
                width: 250,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Red Square, 17',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Entrance 3, intercom 15, apartment 15, floor 5',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 80,
                child: Row(
                  children: const [
                    Icon(
                      Icons.work_outline_outlined,
                      color: Colors.black,
                      size: 35,
                    ),
                    SizedBox(width: 5, height: 5),
                    CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(
                          'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
