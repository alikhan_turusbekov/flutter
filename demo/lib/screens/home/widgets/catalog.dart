import 'package:demo/models/food.dart';
import 'package:demo/screens/details/details.dart';
import 'package:flutter/material.dart';

class Catalog extends StatelessWidget {
  Catalog({Key? key}) : super(key: key);
  final List<Food> foods = [
    Food(
        'Pasta Al Pomodoro with Basil',
        6.50,
        [320, 740, 10, 10, 155],
        'assets/images/one.png',
        'Pasta, extra virgin olive oil, fresh basil, tomatoes, gorlic, black pepper, mixed dry herbs, rock salt, pormigiono reggiano',
        10),
    Food(
        'Pasta Al Ananaso with Onion',
        7.50,
        [135, 240, 05, 15, 200],
        'assets/images/two.png',
        'Pasta, extra virgin olive oil, fresh basil, tomatoes, gorlic, black pepper, mixed dry herbs, rock salt, pormigiono reggiano',
        15),
    Food(
        'Pizza de Teramisu with Lol',
        8.50,
        [420, 480, 20, 15, 140],
        'assets/images/three.png',
        'Pasta, extra virgin olive oil, fresh basil, tomatoes, gorlic, black pepper, mixed dry herbs, rock salt, pormigiono reggiano',
        20),
  ];

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
      width: double.infinity,
      height: queryData.size.height * 0.60,
      color: Colors.white,
      child: Center(
        child: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          itemBuilder: (BuildContext context, int index) {
            return _buildCarousel(context, index ~/ 2);
          },
          itemCount: 1,
        ),
      ),
    );
  }

  Widget _buildCarousel(BuildContext context, int carouselIndex) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          // you may want to use an aspect ratio here for tablet support
          height: queryData.size.height * 0.52,
          child: PageView.builder(
            // store this controller in a State to save the carousel scroll position
            controller: PageController(viewportFraction: 0.90),
            itemBuilder: (BuildContext context, int itemIndex) {
              return _buildCarouselItem(
                  context, carouselIndex, itemIndex, foods);
            },
            itemCount: foods.length,
          ),
        )
      ],
    );
  }

  Widget _buildCarouselItem(BuildContext context, int carouselIndex,
      int itemIndex, List<Food> foods) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(
                top: queryData.size.height * 0.02,
                left: 20,
                right: 20,
              ),
              child: Container(
                decoration: const BoxDecoration(
                  color: Color(0xf4f5f7ff),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                ),
                child: Column(
                  children: [
                    SizedBox(
                        height: queryData.size.height * 0.3,
                        child: const Text('')),
                    Container(
                      padding: const EdgeInsets.all(10),
                      height: queryData.size.height * 0.12,
                      child: Text(
                        foods[itemIndex].name,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28),
                      ),
                    ),
                    SizedBox(
                        height: queryData.size.height * 0.08,
                        child: Container(
                          alignment: Alignment.topLeft,
                          padding: const EdgeInsets.only(left: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Details(food: foods[itemIndex]),
                                ),
                              );
                            },
                            child: Text(
                              '\$' + foods[itemIndex].price.toString() + '0',
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.redAccent,
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: SizedBox(
                height: queryData.size.height * 0.32,
                width: 250,
                child: Hero(
                  tag: foods[itemIndex].name,
                  child: Image.asset(
                    foods[itemIndex].image,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
