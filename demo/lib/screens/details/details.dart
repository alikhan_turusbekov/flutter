import 'package:demo/models/food.dart';
import 'package:demo/screens/details/widgets/body.dart';
import 'package:demo/screens/details/widgets/header.dart';
import 'package:flutter/material.dart';

class Details extends StatelessWidget {
  final Food food;
  const Details({Key? key, required this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Header(
            name: food.name,
          ),
          Body(
            food: food,
          ),
        ],
      ),
    );
  }
}
