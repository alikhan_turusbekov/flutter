import 'package:flutter/material.dart';

class Desc extends StatelessWidget {
  final String desc;
  const Desc({Key? key, required this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Container(
      width: double.infinity,
      height: queryData.size.height * 0.11,
      padding: const EdgeInsets.only(left: 25, top: 5),
      child: Text(
        desc,
        style: const TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
      ),
    );
  }
}
