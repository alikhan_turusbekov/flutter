import 'package:demo/screens/details/widgets/lines.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String name;
  const Header({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return SizedBox(
      width: double.infinity,
      height: queryData.size.height * 0.20,
      child: Center(
        child: Container(
          padding: const EdgeInsets.only(top: 50),
          width: double.infinity,
          child: Column(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  name.split(' ').first,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
              ),
              const Divider(
                color: Colors.transparent,
              ),
              Container(
                width: queryData.size.width,
                padding: const EdgeInsets.only(left: 30),
                child: const Lines(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
