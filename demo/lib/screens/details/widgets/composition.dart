import 'package:flutter/material.dart';

class Composition extends StatelessWidget {
  final List<int> comp;
  const Composition({Key? key, required this.comp}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
      width: double.infinity,
      height: queryData.size.height * 0.14,
      alignment: Alignment.center,
      child: Container(
        height: queryData.size.height * 0.09,
        width: queryData.size.width * 0.9,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
          color: const Color(0xf4f5f7ff),
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(children: [
              Text(
                comp[0].toString(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('gram'),
            ]),
            const VerticalDivider(),
            Column(children: [
              Text(
                comp[1].toString(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('kkal'),
            ]),
            const VerticalDivider(),
            Column(children: [
              Text(
                comp[2].toString(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('белки'),
            ]),
            const VerticalDivider(),
            Column(children: [
              Text(
                comp[3].toString(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('жиры'),
            ]),
            const VerticalDivider(),
            Column(children: [
              Text(
                comp[4].toString(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Text('углеводы'),
            ]),
          ],
        ),
      ),
    );
  }
}
