import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  final String name;
  const Counter({Key? key, required this.name}) : super(key: key);

  @override
  _CounterState createState() => _CounterState(name: name);
}

class _CounterState extends State<Counter> {
  final String name;
  var _counter = 0;
  _CounterState({required this.name});

  void _increment() {
    if (_counter < 100) {
      setState(() {
        _counter++;
      });
    }
  }

  void _decrement() {
    if (_counter > 0) {
      setState(() {
        _counter--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return SizedBox(
      width: double.infinity,
      height: queryData.size.height * 0.1,
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 25),
            width: queryData.size.width * 0.62,
            child: Text(
              name,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
          ),
          Container(
            width: queryData.size.width * 0.38,
            padding: const EdgeInsets.only(right: 5),
            alignment: Alignment.center,
            child: Row(
              children: [
                IconButton(
                    onPressed: _decrement, icon: const Icon(Icons.remove)),
                Container(
                  width: 40,
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.redAccent,
                  ),
                  child: Text(
                    _counter.toString(),
                    style: const TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                IconButton(onPressed: _increment, icon: const Icon(Icons.add))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
