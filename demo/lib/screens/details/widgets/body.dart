import 'package:demo/models/food.dart';
import 'package:demo/screens/details/widgets/composition.dart';
import 'package:demo/screens/details/widgets/counter.dart';
import 'package:demo/screens/details/widgets/desc.dart';
import 'package:demo/screens/footer.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  final Food food;
  const Body({Key? key, required this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    final overallTime = 30 + food.time;

    return GestureDetector(
      onPanUpdate: (details) {
        if (details.delta.dy > 0) {
          Navigator.pop(context);
        }
      },
      child: Container(
        height: queryData.size.height * 0.8,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 15.0,
              offset: Offset(
                0.0,
                -3.0,
              ),
            ),
          ],
        ),
        child: Column(
          children: [
            Container(
              //image
              width: double.infinity,
              height: queryData.size.height * 0.33,
              padding: EdgeInsets.only(
                top: queryData.size.height * 0.03,
              ),
              child: Hero(
                tag: food.name,
                child: Image.asset(
                  food.image,
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Container(
              // timer + time
              width: double.infinity,
              height: queryData.size.height * 0.04,
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                children: [
                  const Icon(
                    Icons.schedule_outlined,
                    size: 24,
                  ),
                  Text(
                    ' + ' + food.time.toString() + ' mins',
                    style: const TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
            Counter(name: food.name),
            Composition(comp: food.comp),
            Desc(desc: food.desc),
            Footer(
                text: overallTime.toString() +
                    ' min, \$' +
                    food.price.toString() +
                    '0'),
          ],
        ),
      ),
    );
  }
}
