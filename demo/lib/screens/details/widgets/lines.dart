import 'package:flutter/material.dart';

class Lines extends StatelessWidget {
  const Lines({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Row(
      children: [
        Container(
          width: queryData.size.width * 0.135,
          height: 3,
          color: Colors.black54,
        ),
        const VerticalDivider(
          color: Colors.transparent,
        ),
        Container(
          width: queryData.size.width * 0.135,
          height: 3,
          color: Colors.grey,
        ),
        const VerticalDivider(
          color: Colors.transparent,
        ),
        Container(
          width: queryData.size.width * 0.135,
          height: 3,
          color: Colors.grey,
        ),
        const VerticalDivider(
          color: Colors.transparent,
        ),
        Container(
          width: queryData.size.width * 0.135,
          height: 3,
          color: Colors.grey,
        ),
        const VerticalDivider(
          color: Colors.transparent,
        ),
        Container(
          width: queryData.size.width * 0.135,
          height: 3,
          color: Colors.grey,
        ),
      ],
    );
  }
}
