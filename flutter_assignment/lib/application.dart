import 'package:flutter/material.dart';

import './texty.dart';

class Application extends StatelessWidget {
  final Function changeText;
  final bool changed;

  Application(this.changeText, this.changed);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Text changer'),
          ),
          body: Texty(
            changeText: changeText,
            changed: changed,
          )),
    );
  }
}
