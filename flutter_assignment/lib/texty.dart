import 'package:flutter/material.dart';

import './textControl.dart';

class Texty extends StatelessWidget {
  final bool changed;
  final Function changeText;

  Texty({this.changeText, this.changed});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          children: [
            changed == false
                ? Text(
                    'One one one one one',
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.red,
                        fontWeight: FontWeight.bold),
                  )
                : Text(
                    'Two two two two two',
                    style: TextStyle(
                        fontSize: 32,
                        color: Colors.blue,
                        fontWeight: FontWeight.w100),
                  ),
            TextControl(changeText)
          ],
        ),
      ),
    );
  }
}
