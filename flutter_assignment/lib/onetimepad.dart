import 'dart:math'; // library to use Random class and to randomize number

class OneTimePad {
  // class that I use for using One Time Pad method
  Random rdn = new Random(); // an object that randomizes
  String key; // Those fields are created —
  String text; // — to check the deciphering dynamically

  String getKey() {
    // to get key that was used in encryption to check decryption
    return this.key;
  }

  String getText() {
    // to get key that was used in encryption to check decryption
    return this.text;
  }

  int _calcToEnc(String a, int b, int i) {
    // all the calculations taken place to calculate the value of ASCII code for character. Let's get one example where we have letter 'l' which has code of 108. then it is minus 96 which ends up as 12. After this we add random number between 1 and 26. For example 15: 12+15=27.We take the mod 26 of 27 which is 1. then we add 96 to give it ASCII code value. In ASCII 97 is 'a'; NOTE: the function does not return letter but its ASCII code.

    if (((a.toLowerCase().codeUnitAt(i) - 96) + b) == 26) return 26 + 96;
    return (((a.toLowerCase().codeUnitAt(i) - 96) + b) % 26) + 96;
  }

  int _calcToDec(String a, String b, int i) {
    // all the calculations taken place to calculate the value of ASCII code for character while decryption. Let's take two random letters: 'c' and 'q'. The ASCII code of 'o' is 99. Then 99-96=3. The ASCII code of 'q' is 113. 113-96=17. After this, we need to substruct 3-17=-14 which less than 1. we add 26 to -14 which ends up as 12. then return 12+96=108. the output letter is 'l'. NOTE: the function does not return letter but its ASCII code.

    var n = (a.toLowerCase().codeUnitAt(i) - 96) -
        (b.toLowerCase().codeUnitAt(i) - 96);
    if (n < 1) n += 26;
    return n + 96;
  }

  String _getChar(int a) {
    // Get the char by its ASCII code. For example: 97-'a', 98-'b' and so on.
    return String.fromCharCode(a);
  }

  int _random1to26() {
    // randomize the number between 1 and 26. The output of nextInt(26) is 0..25 that is why we need to +1.
    return rdn.nextInt(26) + 1;
  }

  void startCiphering(String plainText) {
    //this function that starts the ciphering
    var output = ""; // output variable to store the result of ciphering
    var key =
        ""; // the key variable to store the key that is dynamically generated

    for (int i = 0; i < plainText.length; i++) {
      // the loop to go each character
      var rn = _random1to26(); // the variable to store a randomized number
      key += _getChar(
          rn + 96); // dynamically add each radomized character to the key
      if (plainText.codeUnitAt(i) == 32) {
        // the condition to escape the escape error
        output += ' '; // the replacement for space character
        continue;
      }
      output += _getChar(_calcToEnc(plainText, rn,
          i)); // add the output character to the string holding the value for the overall output
    }

    print("Key: $key");
    print("CipherText: $output");

    this.key = key; // this is to check deciphering
    this.text = output; // this is to check deciphering
  }

  void startDeciphering(String cipherText, String key) {
    // this function starts deciphering
    var output = ""; // the output string that will hold the plaintext

    for (int i = 0; i < cipherText.length; i++) {
      // to go each character of the text
      if (cipherText.codeUnitAt(i) == 32) {
        // the contidion to escape error
        output += ' '; //the replacement for space character
        continue;
      }
      output += _getChar(_calcToDec(cipherText, key,
          i)); // add the output character to the final plaintext
    }

    print("Plain Text: $output");
  }
}

void main() {
  var plainText = "Alikhan Turusbekov";
  var oneTimePad = new OneTimePad();

  print('----Encryption Started!----');
  oneTimePad.startCiphering(plainText);

  print('----Decryption Started-----');
  oneTimePad.startDeciphering(oneTimePad.getText(), oneTimePad.getKey());
}
