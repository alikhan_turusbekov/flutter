import 'package:flutter/material.dart';

class TextControl extends StatelessWidget {
  final Function changeText;

  TextControl(this.changeText);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.black,
      textColor: Colors.white,
      onPressed: changeText,
      child: Text('Change'),
    );
  }
}
