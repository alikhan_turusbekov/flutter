class RSA {
  // a class that is responsible for RSA
// all of the fields were chosen as BigInt to produce calculations with large numbers
  BigInt pn1; // first prime number
  BigInt pn2; // second prime number
  BigInt n; // public key that is pn1*pn2
  BigInt fn; // Ф(n) is given by (pn1-1)*(pn2-1)
  BigInt
      e; // the number between 1 and fn, and should be coprime with n and fn, should be integer
  BigInt d; // private key where d*e mod fn should equal 1
  BigInt k; // some integer k to calculate d

  RSA({this.pn1, this.pn2, this.e, this.k}) {
    // constructor for RSA
    this.n = BigInt.from(
        pn1.toInt() * pn2.toInt()); // the formula to calculate public key
    this.fn = BigInt.from(
        (pn1.toInt() - 1) * (pn2.toInt() - 1)); // the formula to calculate Ф(n)
    this.d = BigInt.from(
        ((k * fn) / e).round()); // the formula to calculate private key
  }

  String startEncryption(String plainText) {
    // the method for encrypting data
    plainText =
        plainText.toLowerCase(); // to put all the characters in lowercase
    var output = ""; // to store the data of output
    for (int i = 0; i < plainText.length; i++) {
      // to go through each character
      if (plainText.codeUnitAt(i) == 32) continue; // to avoid SPACE errors
      output = output +
          (plainText.codeUnitAt(i) - 86)
              .toString(); // all the letters are now numbered from 11-36 (a-z). The reason numbering lettes from 11-36 not from 1-26 is that to ease the process of decryption. For example, if letters numbered from 1 to 26 it is hard to identify whether 112 means AAB or KB or AL
    }
    var a = BigInt.from(int.parse(output)); // to get the BigInt from the String
    var b = a.modPow(e,
        n); // the formula to produce cipher the string output to the power of e mod n
    return b.toString(); // return String of Cipher
  }

  String startDecryption(String cipherText) {
    // the method for decrypting data
    var output = ""; // to store the output data
    var a = BigInt.from(int.parse(cipherText)); // convert the cipher to BigInt
    a = a.modPow(d, n); // cipher to the power of d mod n
    var b = a.toString(); // convert the result to String
    int i = 0; // iterator
    while (i < b.length) {
      // until all the length is all went through
      var c = b.substring(i,
          i + 2); //to get each letter because each letter is represented by two numbers (11-36)
      i = i + 2; //move iterator
      output = output +
          String.fromCharCode(
              86 + int.parse(c)); // get the char from the number
    }
    return output; // return plainText
  }
}

void main() {
  // main
  var pn1 = BigInt.from(53); // assigning first primeNumber
  var pn2 = BigInt.from(59); // assigning second primeNumber
  var e = BigInt.from(3); // assigning e
  var k = BigInt.from(2); // assigning k

  var plaintext = 'as';
  var cipher = '2400';

  print('the cipher for the plaintext of $plaintext is —');
  print(RSA(e: e, pn1: pn1, pn2: pn2, k: k)
      .startEncryption(plaintext)); // encryption of plainText using RSA

  print('the plaintext for the cipher of $cipher is —');
  print(RSA(e: e, pn1: pn1, pn2: pn2, k: k)
      .startDecryption(cipher)); // decryption of cipher using RSA
}
