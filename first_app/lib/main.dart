import 'package:first_app/result.dart';
import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'What will you choose?',
      'answers': [
        {'text': 'Money', 'score': 25},
        {'text': 'Love', 'score': 5},
        {'text': 'Knowledge', 'score': 0}
      ],
    },
    {
      'questionText': 'What\'s 33+77?',
      'answers': [
        {'text': '120', 'score': 25},
        {'text': '100', 'score': 25},
        {'text': '110', 'score': 0}
      ],
    },
    {
      'questionText': 'Who are you?',
      'answers': [
        {'text': 'Adventurous eater', 'score': 25},
        {'text': 'Non-adventurous eater', 'score': 0},
        {'text': 'Let me think first...', 'score': 0}
      ],
    },
    {
      'questionText': 'Rate your beauty',
      'answers': [
        {'text': '10/10', 'score': 10},
        {'text': '100000000000/10', 'score': 25},
        {'text': '0/10', 'score': 0}
      ],
    },
  ];

  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;

    setState(() {
      _questionIndex++;
    });

    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Introvert vs Extrovert?'),
          ),
          body: _questionIndex < _questions.length
              ? Quiz(
                  answerQuestion: _answerQuestion,
                  questionIndex: _questionIndex,
                  questions: _questions,
                )
              : Result(_totalScore, _resetQuiz)),
    );
  }
}
